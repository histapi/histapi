#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from queue import Queue
import logging


class HistapiPathError(Exception):
    pass

class Histapi:   
    def _assign_keys(self, config):
        key_actions_menu_map = {}

        _BUTTON_PAUSE = config['GPIO']['button_pause']
        _BUTTON_RIGHT = config['GPIO']['button_right']
        _BUTTON_LEFT = config['GPIO']['button_left']
        _BUTTON_HOME = config['GPIO']['button_home']
        key_actions_menu_map[_BUTTON_LEFT] = {
            'label': 'left', 'action': 'action_left', 'menu': 'menu_left'}
        key_actions_menu_map[_BUTTON_RIGHT] = {
            'label': 'right', 'action': 'action_right', 'menu': 'menu_right'}
        key_actions_menu_map[_BUTTON_HOME] = {
            'label': 'home', 'action': 'action_home', 'menu': 'menu_home'}
        key_actions_menu_map[_BUTTON_PAUSE] = {
            'label': 'pause', 'action': 'action_pause', 'menu': 'menu_pause'}

        if config.getboolean('OPTIONS', 'is_pi'):
            from gpiozero import Button
            pull_up_setting = config.getboolean('GPIO', 'pull_up')

            self.left_button = Button(_BUTTON_LEFT, pull_up=pull_up_setting)
            self.right_button = Button(_BUTTON_RIGHT, pull_up=pull_up_setting)
            self.home_button = Button(_BUTTON_HOME, pull_up=pull_up_setting)
            self.pause_button = Button(_BUTTON_PAUSE, pull_up=pull_up_setting)
        return key_actions_menu_map

    def _set_paths(self, config):
        from pathlib import Path
        # dummyrec = config.getboolean('OPTIONS', 'dummy_rec')
        main_path = Path(config['PATH']['main_path'])
        story_folder = Path(config['PATH']['story_folder'])
        audio_titles_folder = Path(config['PATH']['audio_titles_folder'])

        # full paths        
        self.full_paths = {
            'story_path' : main_path / story_folder,
            'sfx_path' : main_path / 'SFX',
            'audio_titles_path' : main_path / audio_titles_folder,
            'rec_workdir' : main_path / 'rec_work'
        }
    
        # instance attributes are dynamically set from the full_paths dict
        for path_name, path_value in self.full_paths.items():
            setattr(self, path_name, path_value)

        # self.story_path = main_path / story_folder
        # self.sfx_path = main_path / 'SFX'
        # self.audio_titles_path = main_path / audio_titles_folder
        # self.rec_workdir = main_path / 'self.rec_workdir'

    def _check_folders(self):
        from pathlib import Path
        for path in self.full_paths.values():
            if not Path(path).exists():
                raise HistapiPathError(
                    f"The path {path} does not exist, cannot continue")

    def __init__(self):
        import configparser
        import sys

        # read config
        config = configparser.ConfigParser(allow_no_value=True)
        config.read('histapi.conf')

        self._set_paths(config)
        try:
            self._check_folders()
        except HistapiPathError as e:
            sys.exit(e)

        # keys config
        key_actions_menu_map = self._assign_keys(config)

        from histapi import Playlist

        from histapi import actions

        from histapi import Menu

        root_menu = Menu.Menu(
            0,
            name='Root Menu',
            sfx_path=self.sfx_path,
            audio_title_path=self.audio_titles_path / 'home.wav',
        )

        nav_story = Menu.Menu(
            0,
            parent=root_menu,
            name='nav_story',
            audio_title_path=self.audio_titles_path / 'navigate_story.wav',
            menu_home=root_menu,
            action_pre=actions.play_header,
            action_pause=actions.play_story,
            action_right=actions.next_story,
            action_left=actions.prev_story
        )

        play_story = Menu.Menu(
            0,
            parent=nav_story,
            name='story Playback',
            sfx_path=self.sfx_path,
            menu_home=nav_story,
            action_pause=actions.toggle_playback,
            action_home=actions.stop_story,
            # in all cases we need to stop playback when leaving this menu
            action_post=actions.stop_story
        )

        delete_story = Menu.Menu(
            0,
            parent=play_story,
            name='delete story',
            sfx_path=self.sfx_path,
            menu_home=nav_story,
            # will execute delete_story then go to navigation
            action_pause=actions.delete_story,
            menu_pause=nav_story
        )

        rec_story = Menu.Menu(
            0,
            parent=root_menu,
            name='rec_story',
            menu_home=root_menu,
            action_pre=actions.show_story_info,
            action_left=actions.start_record,
            action_pause=actions.validate_story,
            menu_pause=root_menu
        )

        rec_story_in_progress = Menu.Menu(
            0,
            parent=rec_story,
            name='rec_story (progress)',
            menu_home=root_menu,
            action_left=actions.stop_record
        )

        listen_choice = Menu.Menu(
            0,
            parent=rec_story,
            name='listen_choice',
            action_left=actions.listen_take,
            action_right=actions.not_listen_take,
            # action_pause=actions.validate_story,
            menu_pause=root_menu
        )

        take_choice = Menu.Menu(
            0,
            parent=listen_choice,
            name='take_choice',
            action_left=actions.try_again_record,
            menu_left=rec_story,
            action_right=actions.validate_take,
            menu_right=rec_story,
            action_pause=actions.cancel_take,
            menu_pause=rec_story
        )

        # adding more menu links now that all menus has been instantiated

        # root_menu dispatch to record or navigation
        root_menu.update_actions_menus('menu_left', rec_story)

        playlist = Playlist.Playlist(self.story_path)

        if playlist.data:
            root_menu.update_actions_menus('menu_pause', nav_story)
        else:
            # we warn the user there is no story yet
            root_menu.update_actions_menus(
                'action_pre', actions.warn_playlist_empty)
            # navigation menu is disconnected and we inform the user
            # when he tries to enter it
            root_menu.update_actions_menus(
                'action_pause', actions.warn_playlist_empty)
            # TODO: reconnect the navigation menu whenever a story is recorded

        # in navigation, pause button link to play_story
        nav_story.update_actions_menus('menu_pause', play_story)
        # in play_story, left button link back to nav_story
        play_story.update_actions_menus('menu_left', delete_story)

        # in listen_choice, left button lead to take_choice menu after
        # listening (or not) the take
        listen_choice.update_actions_menus('menu_left', take_choice)
        listen_choice.update_actions_menus('menu_right', take_choice)

        rec_story.update_actions_menus('menu_left', rec_story_in_progress)
        # once recorded, choice is to 1) listen it and save it  2) just
        # save it 3) back to root menu
        rec_story_in_progress.update_actions_menus('menu_left', listen_choice)

        if config.getboolean('OPTIONS', 'is_pi'):
            wait_handler = self.wait_button
        else:
            wait_handler = self.wait_button_pc

        run_args = {
            'playlist': playlist,
            'wait_button_handler': wait_handler,
            'key_actions_menu_map': key_actions_menu_map
        }

        next_menu = root_menu.run(**run_args)
        self.record_in_progress = False

        # MAIN LOOP
        while True:
            if next_menu.name == 'rec_story':
                from histapi import Record
                # we don't want to drop the record object when recording
                # takes and loop over the record menu again an again
                if not self.record_in_progress:
                    record = Record.Record(
                        self.rec_workdir,
                        self.story_path,
                        playlist.last_self_story_number)
                    self.record_in_progress = True
                # if record object don't have chapter yet, validate_story
                # loops back to rec_story & warn user
                if record.chapters_sounds:
                    rec_story.update_actions_menus(
                        'action_pause', actions.validate_story)
                    rec_story.update_actions_menus('menu_pause', root_menu)
                else:
                    logging.info('//// no chapter sounds yet')
                    rec_story.del_actions_menus('menu_pause')
                    rec_story.update_actions_menus(
                        'action_pause', actions.warn_no_chapter)
                # pass record
                run_args['record'] = record
            if next_menu.name == 'root_menu' and self.record_in_progress:
                # when recording finish, we invert the record flag.
                # The record object will be overwritten for next record
                self.record_in_progress = not self.record_in_progress
                del run_args['record']

            next_menu = next_menu.run(**run_args)

    def wait_button_pc(self):
        from click import getchar
        print('wait for button (PC)...')
        # For PC keyboard
        c = getchar()
        return c

    def wait_button(self):
        queue = Queue()
        # start = timer()

        print(self.left_button)
        self.left_button.when_pressed = queue.put
        self.right_button.when_pressed = queue.put
        self.home_button.when_pressed = queue.put
        self.pause_button.when_pressed = queue.put

        # end = timer()
        # print(end - start)

        print('wait')
        e = queue.get()

        self.left_button.when_pressed = None
        self.right_button.when_pressed = None
        self.home_button.when_pressed = None
        self.pause_button.when_pressed = None

        return str(e.pin.number)
