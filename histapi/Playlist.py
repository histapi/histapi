#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from shutil import rmtree
from histapi import playlist_utils
import logging


class Playlist:
    """
        >>> import Playlist
        >>> p = Playlist.Playlist('mystories')
        >>> p = Playlist.Playlist('mystories_wrong')
        Traceback (most recent call last):
            ...
        FileNotFoundError: [Errno 2] No such file or directory: 'mystories_wrong'
    """
    cur_pos = 0
    story_is_paused = False

    @property
    def cur_story_data(self):
        return self.data[self.cur_pos]

    # @cur_story_data.setter
    # def cur_story_data(self):

    def __init__(self, storypath):
        self.data, self.end_index, self.last_self_story_number = playlist_utils.scan_for_story(storypath)
        self.storypath = storypath

    def delete_story(self):
        try:
            rmtree(self.cur_story_data['path'])
        except OSError:
            print('problem at removing the story folder')
        else:
            self.data, self.end_index, self.last_self_story_number = playlist_utils.scan_for_story(self.storypath)

        if self.cur_pos > self.end_index:
            # we have removed the last story of the playlist !
            # and we don't want the current item to be out of range
            # the current item will be the latest one
            self.cur_pos = self.end_index

    def switch_pos(self, direction, step):
        # print("current position: {}, story: {}".format(self.cur_pos, self.data[self.cur_pos]['name']))
        """
        Points the index to another story.

        It also manages the limits of the requested index position.

        :param direction (str): going forward or backward
        :param step (int): how many steps. number of stories to go within the direction

        :returns: False if failed

        >>> import Playlist
        >>> p = Playlist.Playlist('mystories')
        >>> p.switch_pos('forward', -1)
        False
        >>> p.cur_pos
        0
        >>> p.switch_pos('forward', 1.9)
        False
        >>> p.switch_pos('forward', 1)
        >>> p.cur_pos
        1
        """
        if type(step) is not int:
            logging.error("Step should be an integer")
            return False
        if step < 0:
            logging.error("Negative step is not possible")
            return False
        if direction == 'backward':
            new_pos = self.cur_pos - step
            if new_pos < 0:
                logging.warning("Cannot go backward (beginning of playlist)")
                new_pos = 0
                return False
            self.cur_pos = new_pos
        elif direction == 'forward':
            new_pos = self.cur_pos + step
            if new_pos > self.end_index:
                logging.warning("Cannot go forward (end of playlist)")
                new_pos = self.end_index
                return False
            self.cur_pos = new_pos
        else:
            print("direction should be \"backward\" or \"forward\"")
            return False

    def show_info(self):
        """
        Show various data about the playlist and the position of the cursor
        """
        story_names = []
        for h in self.data:
            story_names.append(h['name'])

        print("current position is {}".format(self.cur_pos))
        print("There are {} story".format(len(story_names)))
        if len(story_names):
            print("They are : {}".format(story_names))
            print("The current story is {}".format(story_names[self.cur_pos]))
            print("\n")
