#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from os import makedirs, system, WEXITSTATUS
from pathlib import Path
from shutil import copy


class Record:
    base_self_story_name = 'mystory-'
    chapter_base_name = 'ch-'
    chapter_cur_idx = 1
    chapter_extension = 'wav'
    story_processed_filename = 'story_processed.wav'
    chapters_sounds = []

    def cleanup_dir(self, dir):
        for item in Path(dir).iterdir():
            item.unlink()

    def __init__(self, rec_workdir, story_path, last_self_story_number):
        # might we want to scan the rec_workdir folder to see if there
        # are pending items ?
        
        if not Path(rec_workdir).is_dir():
            makedirs(rec_workdir)
        # self.cleanup_dir(rec_workdir)
        self.rec_workdir = rec_workdir
        self.story_path = story_path
        self.last_self_story_number = last_self_story_number

    @property
    def cur_chapter_file(self):
        return self.rec_workdir / '{}{}.{}'.format(
               self.chapter_base_name, self.chapter_cur_idx, self.chapter_extension)

    def delete_cur_chapter(self):
        print('remove {}'.format(self.cur_chapter_file))
        Path(self.cur_chapter_file).unlink()

    def next_chapter(self):
        self.chapter_cur_idx += 1
        print('next_chapter done {}'.format(self.chapter_cur_idx))

    def store_take(self):
        self.chapters_sounds.append(self.cur_chapter_file)

    def concat_chapters(self):
        print('{} chapter(s)'.format(len(self.chapters_sounds)))
        if len(self.chapters_sounds) == 1:
            copy(self.chapters_sounds[0],
                 '{}/story_concat_raw.wav'.format(self.rec_workdir))
        else:
            # the story is made from several chapters
            system('sox {} {}/story_concat_raw.wav'.format(
                   ' '.join([str(_) for _ in self.chapters_sounds]),
                   self.rec_workdir))

        # A little audio processing
        _ = system('sox {0}/story_concat_raw.wav {0}/story_processed.wav \
                    contrast norm'.format(self.rec_workdir))
        if WEXITSTATUS(_) == 0:
            print("Audio Processing OK")
            return True
        else:
            return False

    def install_processed_story(self):
        copy(self.story_processed_filename, '{}/{}-{}'.format(
            self.storypath,
            self.base_self_story_name,
            self.last_self_story_number)
        )
    
    def create_header(self):
        pass
