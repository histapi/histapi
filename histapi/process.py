#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from histapi import config

# print(chapter)

def make_record(last_self_story_number):
    """
        make_record lance Sox qui enregistre le son venant du micro hw:1,0
        make_record crée le repertoire qui va accueillir le /les fichiers d'enregistrement.
        L'index "n" s'auto régule en fonction de la taille de la playlist.
        D'où l'importance d'updater cette playlist avant chaque création de repertoire et/ou de fichiers.
    """

    print ("Démarrage de l'enregistrement")
    os.system("aplay {}rec_start.wav".format(config.maintitlespath))
    os.system("mkdir {}mystory-{}".format(config.storypath, last_self_story_number + 1))
    if config.dummyrec:
        os.system("sox -n -r 8000 storybrut.wav synth 15 sine 500 &")
    else:
        os.system("AUDIODEV=hw:1,0 AUDIODRIVER=alsa rec -q storybrut.wav &")

def break_record():
    """ break_record kill le processus Sox et ce faisant met fin à l'enregistrement """

    os.system('pkill rec')
    os.system("aplay {}rec_stop.wav".format(config.maintitlespath))
    print("enregistrement arrêté")

def processing_record(last_self_story_number):
    """
        processing_record utilise Sox pour une augmentation de volume de l'enregistrement
        TODO
        A adapter selon l'utiliateur et son équipement????
    """

    print("Veuillez patientez, traitement du son encours")

    # Sox prend les deux premieres secondes ('trim 0 2')  du son pour constituer le profil du bruit
    os.system("sox {}story_unify.wav -n trim 4 6 noiseprof speech.noise-profile".format(config.mainpath))

    # Sox enlève le profil sur l'ensemble du son, avec une intensité de 0.2
    os.system("sox {}story_unify.wav storyclean.wav noisered speech.noise-profile 0.2".format(config.mainpath))

    # Sox augmente l'amplitude du son 'vol 2'
    os.system("sox {}storyclean.wav storycontrast.wav contrast".format(config.mainpath))
    os.system("sox {}storycontrast.wav story.wav --norm=0".format(config.mainpath))

    # suppression des fichiers temporaires
    os.system("rm {}story_unify.wav".format(config.mainpath))
    os.system("rm {}storyclean.wav".format(config.mainpath))
    os.system("rm {}storycontrast.wav".format(config.mainpath))
    os.system("rm {}speech.noise-profile".format(config.mainpath))
    print ("Traitement du son effectué")

    # nous transférons l'histoire réunifiée dans le repertoire de data adéquats pour le scan PlaylistUtils
    os.system("mv {}story.wav {}mystory-{}".format(config.mainpath, config.storypath, last_self_story_number + 1))
    print('Transfert des fichiers dans le repertoire mystory')


def make_title(last_self_story_number):
    """ make_title crée le titre de l'histoire à partir des 4 premieres secondes de l'enregistrement(trim 0 4) """

    print ('création du titre')
    os.system('aplay {}make_title.wav'.format(config.maintitlespath))
    os.system("sox {}mystory-".format(config.storypath) + str(last_self_story_number + 1) + "/story.wav {}mystory-".format(config.storypath) + str(last_self_story_number + 1) +"/title.wav trim 0 4")
    print ('titre créé')



def play_record(last_self_story_number):
    """ play_record lit l'enregistrement qui vient d'être effectué avant enregistrement par l'uilisateur """


    print ("Lecture de l'histoire {} enregistrée".format(last_self_story_number + 1))
    os.system('aplay {}storybrut.wav'.format(config.mainpath))
    print ('lecture terminée')



def confirmed_chapter_record(chapter):
    """
        confirmed_chapter_record modifie le nom "story" de l'enregistrement
        en nom de chapitre "story_chapter" en lui assignant son numéro
    """

    # global chapter
    os.system('aplay {}confirmed_chapter_record.wav'.format(config.maintitlespath))
    os.system("mv {}storybrut.wav story_chapter".format(config.mainpath) + str(chapter) +".wav")

    print('chapitre' + str(chapter) + ' enregistré')



def delete_record(last_self_story_number, chapter):
    """
        delete_record détruit l'enregistrement qui vient d'être effectué à la demande de l'utilisateur
        un titre n'a pas encore été créé
        donc la playlist ne détecte pas encore la présence de ce nouveau son. 'n' est toujours le meme
    """

    #destruction du repertoire créé pour l'enregistrement
    os.system('rm -rf {}mystory-{}'.format(config.storypath, last_self_story_number + 1))

    #destruction de l'enregistrement effectué

    os.system('rm {}story_chapter'.format(config.mainpath) + str(chapter) + '.wav')
    print ('enregistrement annulé')
    os.system("aplay {}delete_record.wav".format(config.maintitlespath))


def confirmed_record(last_self_story_number):
    """
        confirmed_record confirme à l'utilisateur que son enregistrement est validé et
        envoie vers le processus d'unification des parties

    """

    print ('enregistrement confirmé')
    unify_playlist_chapter(last_self_story_number)
    processing_record(last_self_story_number)
    print ("retour au sommaire")
    os.system("aplay {}confirmed_record.wav".format(config.maintitlespath))



def unify_playlist_chapter(last_self_story_number):
    """
        unify_playlist_chapter a pour mission de réunir les morceaux d'histoires enregistrés

        Pour cela on utilise la fonction de concaténation de Sox
        On utilise un système tiré par les cheveux
        pour indiquer à Sox quels vont être les fichiers à concaténer (en attente de mieux)

    """

    # on indique d'aller chercher playlistconfig.mainpath, liste initiale déterminée en début de programme issue du scan du config.mainpath
    playlist_main_path = os.listdir(config.mainpath)

    os.system('aplay {}unify_chapter_record.wav'.format(config.maintitlespath))
    print("unification des chapitres en une histoire")

    # on crée une liste "playlist_chapter" à partir du scan actuel du config.mainpath, comportant à présent les chapitres d'histoires
    # précédemment enregistrés

    playlist_chapter = os.listdir(config.mainpath)

    # on trie la liste
    playlist_chapter.sort()
    print(playlist_chapter)
    print(playlist_main_path)

    # on crée une nouvelle liste "playlist_chapter_only" en soustrayant au contenu de la liste actuelle le contenu intial
    # cette liste contient donc uniquement les chapitres précédemment enregistrés

    playlist_chapter_only = list(set(playlist_chapter)-set(playlist_main_path))
    # on trie cette liste
    playlist_chapter_only.sort()
    print(playlist_chapter_only)

    # on enlève à la playlist_chapter_only toute sa ponctuation afin de pouvoir indiquer à os.system ('Sox ...') la bonne syntaxe
    playlist_chapter_ready = ''
    for e in playlist_chapter_only:
        playlist_chapter_ready = playlist_chapter_ready + ' ' + e
    print(playlist_chapter_ready)

    # nous avons la bonne syntaxe, nous lancons la concaténation des chapitres avec os.system ('sox .... .... .... story.wav)
    os.system("sox " + playlist_chapter_ready + " story_unify.wav")

    # nous transférons l'histoire réunifiée dans le repertoire de data adéquats pour le scan PlaylistUtils
    # os.system("mv {}story.wav {}mystory-{}".format(config.mainpath, config.storypath, last_self_story_number + 1))
    # suppression des fichiers chapitres
    os.system("rm " + playlist_chapter_ready)
    print('unification effectuée')
