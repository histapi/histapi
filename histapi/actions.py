#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from . import helpers

import logging
import pprint
pp = pprint.PrettyPrinter()


def play_header(menu, playlist):
    print('playing header')
    helpers.playSFX(playlist.cur_story_data['header'])


def next_story(menu, playlist):
    print('action next')
    playlist.switch_pos('forward', 1)
    playlist.show_info()

    helpers.playSFX(playlist.cur_story_data['header'])


def prev_story(menu, playlist):
    print('action previous')
    playlist.switch_pos('backward', 1)
    playlist.show_info()

    helpers.playSFX(playlist.cur_story_data['header'])


def play_story(menu, playlist):
    print('action previous')
    playlist.show_info()

    helpers.play_stream(playlist.cur_story_data['sound'])


def delete_story(menu, playlist):
    print('delete story')
    playlist.delete_story()


def toggle_playback(menu, playlist):
    print('pause_playback')
    if playlist.story_is_paused:
        helpers.unpause_story_playback(menu)
    else:
        helpers.pause_story_playback(menu)

    playlist.story_is_paused = not playlist.story_is_paused


def stop_story():
    helpers.stop_story()
    print('stop_playback')


def warn_playlist_empty(menu, playlist, *mixed):
    # TODO : play a sound
    print('No story available !')
    print(menu.sfx_path)
    helpers.playSFX(menu.sfx_path / 'blip-c-02.wav')

def show_story_info(menu, playlist, record):
    print('the current chapter file is {}'.format(record.cur_chapter_file))
    print('chapters list: {}'.format(record.chapters_sounds))


def start_record(menu, playlist, record):
    print('action start_record in {}'.format(record.cur_chapter_file))
    os.system('pkill -u pi rec')
    helpers.stop_stream()
    os.system('AUDIODEV=hw:1,0 AUDIODRIVER=alsa rec {} &'.format(record.cur_chapter_file))
    # _ = os.system('sox -n -c 1 -b16 -r 48000 {} synth 5 sine 500 vol 0.7 &'
              # .format(record.cur_chapter_file))
    # if os.WEXITSTATUS(_) == 0:
        # print('record OK')

def stop_record(menu, playlist, record):
    print('action stop_record')
    os.system('pkill rec')


def try_again_record(menu, playlist, record):
    print('try again record')
    record.delete_cur_chapter()
    start_record(menu, playlist, record)


def validate_take(menu, playlist, record):
    print('store_take')
    record.store_take()
    record.next_chapter()


def validate_story(menu, playlist, record):
    print('validate_story')
    # join all chapters and process
    _ = record.concat_chapters()
    # install the story
    if _:
        record.install_story()
    else:
        logging.error("concat chapters went wrong...")


def warn_no_chapter(menu, playlist, record):
    print('warn_no_chapter')


def listen_take(menu, playlist, record):
    print('listen_take {}'.format(record.cur_chapter_file))
    helpers.play_stream(record.cur_chapter_file)
    # helpers.play_stream('/usr/share/sounds/speech-dispatcher/dummy-message.wav')


def cancel_take(menu, playlist, record):
    print('cancel Take')
    record.delete_cur_chapter()


def not_listen_take(menu, playlist, record):
    print('not_listen_take')
