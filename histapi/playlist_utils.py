#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import sys
# import mutagen
import logging
from operator import itemgetter
from pathlib import Path

import pprint
pp = pprint.PrettyPrinter()

logging.basicConfig(level=logging.INFO)


def sort_playlist(playlist):
    """
    Sort the playlist by alphanumeric order while moving self recorded stories
    at the end of the list

    For instance:
    - RobinDesBois
    - 101Dalmatiens
    - LesCrocodiles
    TODO
    """
    logging.info('sorting playlist...')

    not_moving = []
    moving_to_end = []

    # split the playlist in two
    # one with 'mystory-'
    # one with the rest

    for s in playlist:
        if s['name'].startswith('mystory-'):
            moving_to_end.append(s)
        else:
            not_moving.append(s)

    # sort the two lists separatly
    not_moving.sort(key=itemgetter('name'))
    moving_to_end.sort(key=itemgetter('name'))

    # the new playlist is the concatenation of the two sorted playlists
    playlist.clear()
    playlist.extend(not_moving)
    playlist.extend(moving_to_end)

    if len(moving_to_end):
        last_self_story_number = int(moving_to_end.pop()['name'].lstrip('mystory-'))
    else:
        last_self_story_number = 0

    return playlist, last_self_story_number


def scan_for_story(stories_path):
    """
    Scan a path for stories folders and make sure all the required files exists
    Returns a list of dict elements, containing the metadata for each story.
    """

    # this dict is only used as a template.
    # it is used when checking expected files for a story
    element_schema = {'sound': 'story.wav', 'header': 'title.wav'}

    # the final playlist data
    playlist = []
    
    if not Path(stories_path).exists():
        logging.error("the stories folder {} doesn't exist".format(stories_path))
        sys.exit(1)
        
    with os.scandir(stories_path) as it:
        for entry in it:
            # stories are saved in subfolders
            if entry.is_dir():
                # we will be working in a copy of the schema, so we can update it
                # as needed, as it goes
                story_element = element_schema.copy()
                story_name = entry.name
                story_folder = entry.path
                logging.debug("folder: {}".format(entry.name))
                with os.scandir(entry) as it:
                    # we store the files refs that are found in the current folder
                    files_found = {}
                    # those are the files that we require to find
                    expected_files = list(element_schema.values())
                    for entry in it:
                        if not entry.name.startswith('.') and entry.is_file():
                            # as key we have the filename
                            # as value we have the full path of it
                            files_found[entry.name] = entry.path
                    logging.debug('files_found: {}'.format(list(files_found.keys())))
                    logging.debug('expected_files: {}'.format(expected_files))
                    # compare the list of files found with the required one
                    if set(list(files_found.keys())) == set(expected_files):
                        logging.info('# all required files have been found !')
                        # we store the *full path* of the files.
                        for k, v in element_schema.items():
                            story_element[k] = files_found[v]

                        story_element['name'] = story_name
                        story_element['path'] = story_folder
                        # adding meta
                        # TODO : mutagen can raise an exception
                        # if the file isn't MP3 content
                        # m = mutagen.File(story_element['sound'])
                        # story_element['story_length'] = m.info.length
                        # m = mutagen.File(story_element['header'])
                        # story_element['header_length'] = m.info.length
                        # finally storying the element to the playlist
                        playlist.append(story_element)
                    else:
                        # we fall here when one of the required file for the story
                        # is missing.
                        logging.warning('the following files are missing in {}'.format(story_name))
                        logging.warning(set(list(files_found.keys())) ^ (set(expected_files)))

    if len(playlist):
        end_index = len(playlist) - 1
        playlist, last_self_story_number = sort_playlist(playlist)
    else:
        end_index = 0
        last_self_story_number = 0

    return playlist, end_index, last_self_story_number
