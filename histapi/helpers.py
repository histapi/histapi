#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# init sound playback
from pygame import mixer
mixer.init(48000, -16, 1, 1024)
channelSFX = mixer.Channel(1)

# TODO : preload all AUI sound files to avoid a call to mixer.Sound()


def play_stream(file):
    print('play stream')
    print(type(file))
    mixer.music.load(str(file))
    mixer.music.play()


def playSFX(file):
    print("play SFX")
    channelSFX.play(mixer.Sound(str(file)))


def pause_story_playback(menu):
    mixer.music.pause()
    # blip indicating that pause took effect
    channelSFX.play(mixer.Sound(str(menu.sfx_path / 'blip-c-02.wav')))


def unpause_story_playback(menu):
    mixer.music.unpause()


def stop_stream():
    mixer.music.stop()
