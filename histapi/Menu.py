#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# from queue import Queue
import sys
from . import helpers
import pprint
pp = pprint.PrettyPrinter()


class Menu:
    def __repr__(self):
        return "{} object".format(self.name)

    def __init__(self, pl_pos, name, parent=None, audio_title_path=None, sfx_path=None, **actions_menus):
        print('init {}'.format(name))
        # for the dynamic view
        self.actions_menus = actions_menus
        self.actions_menus_view = self.actions_menus.keys()
        # TODO explain this
        self.actions, self.menus = self.split_actions_menus(actions_menus)
        self.audio_title_path = audio_title_path
        self.sfx_path = sfx_path

        # root menu is special
        if parent is None:
            # if 'action_home' not in self.actions:
            #     raise Exception('The root menu should have an action_home set')
            self.actions['action_home'] = self._quitHistapi
        else:
            # To exist, a menu need at least one further menu choice
            # ortherwise there would be no way to get out of it
            if not(self.menus):
                raise Exception('Menu object needs at least one menu choice')

        self.parent = parent
        self.name = name
        self.pl_pos = pl_pos

    def update_actions_menus(self, type, func_ref):
        print('in add_actions_menus')
        if type in self.actions_menus_view:
            print('Warning: this action/menu already exists. Will be redefined in the Menu')
        # warn & overwrite action/menu
        if type.startswith('menu_'):
            self.menus[type] = func_ref
        else:
            self.actions[type] = func_ref

        self.actions_menus[type] = func_ref


    def del_actions_menus(self, type):
        if type not in self.actions_menus_view:
            print('Error: this action/menu does not exist in the Menu')
            return False
        else:
            if type.startswith('menu_'):
                del self.menus[type]
            else:
                del self.actions[type]

            del self.actions_menus[type]

    def split_actions_menus(self, actions_menus):
        actions = {}
        menus = {}
        for k, v in actions_menus.items():
            if k.startswith('menu_'):
                menus[k] = v
            else:
                actions[k] = v
        return actions, menus

    def _quitHistapi(self, *args):
        print('quit ! (we were in {})'.format(self))
        sys.exit(0)

    def run(self, playlist, wait_button_handler, key_actions_menu_map, record=False):
        print('\n')
        print('##### running menu "{}" #####'.format(self.name))
        print('actions & menus:')
        pp.pprint(self.actions_menus)

        # play menu title
        # Audio title is optional
        if self.audio_title_path:
            helpers.playSFX(self.audio_title_path)

        # build params for action
        mixed_params = [self, playlist]

        if record:
            mixed_params.append(record)

        if 'action_pre' in self.actions:
            print('exec pre action')
            self.actions['action_pre'](*mixed_params)

        while True:
            btn = wait_button_handler()

            try:
                # pp.pprint(key_actions_menu_map)
                btn_label = key_actions_menu_map[btn]['label']
                btn_action = key_actions_menu_map[btn]['action']
                btn_menu = key_actions_menu_map[btn]['menu']
            except KeyError as e:
                print('button not mapped: {}'.format(e))
                continue

            btn_action_menu = [btn_action, btn_menu]
            

            if all([item in self.actions_menus_view for item in btn_action_menu]):
                print('both menu and action')
                # print('action: {}'.format(self.actions))
                # print('menu: {}'.format(self.menus))
                # both action and menu defined for the button. Will return the menu after action execution
                self.pl_pos = self.actions[key_actions_menu_map[btn]['action']](*mixed_params)
            elif btn_menu in self.menus:
                print('menu only ({})'.format(btn_menu))
            elif btn_action in self.actions:
                print('action only ({})'.format(btn_action))
                self.pl_pos = self.actions[key_actions_menu_map[btn]['action']](*mixed_params)
                continue
            else:
                print("no action or menu mapped to button '{}'!".format(btn))
                continue

            # we execute the provided post action and point to next menu
            if 'action_post' in self.actions:
                self.actions['action_post'](*mixed_params)
            return self.menus[key_actions_menu_map[btn]['menu']]
