# Histapi - your stories in your pi (zero)
The idea of this project is to provide the software and basic 
guidelines to build a free story box, intended for children but 
**assembled by an adult**. The sourcecode provided in the current 
project doesn't rely on any proprietary software, however and without 
going into details remember that the RaspBerry PI isn't a open-hardware 
board (neither as most of the computers down there). For instance [it 
requires non-free software to 
boot-up](https://www.fsf.org/resources/hw/single-board-computers)

**Remember** : YOU and YOUR CHILDREN use it at your own risk !

# Basic hardware schematic

![basic setup](Doc/histapi-minimal-mini.png)

The button wiring is for a pull-down configuration (the electrical 
level is bound to 0V when the button is released). In this setup, 
`pull_up` should be set to `no` in `histapi.conf`

# Setup

## Installing RASPIAN LITE

- Download Raspian Lite at 
https://www.raspberrypi.org/downloads/raspberry-pi-os/ (currently 
buster)
- Choose and use your favorite [installation 
method](https://www.raspberrypi.org/documentation/installation/installing-images/README.md)
- Before booting on the freshly created SD card, configure wifi and 
SSH. To achieve this:
  1. create an empty file `ssh` in the /boot partition - [official 
documentation](https://www.raspberrypi.org/documentation/remote-access/ssh/)
  1. Open `/etc/wpa_supplicant/wpa_supplicant.conf` and provide your wifi 
configuration - [official 
documentation](https://www.raspberrypi.org/documentation/configuration/wireless/headless.md)

## First boot
1. Initiate a SSH connection to the PI. You should findout its IP 
address in the [official 
documentation](https://www.raspberrypi.org/documentation/remote-access/ip-address.md).
1. Tweak the system language & locale if needed : `raspi-config` 
command then `localisation option` -> `set locals`.
1. Change default password (`raspberry`) with a new one using `passwd` 
command
1. Update the system : `sudo apt-get update && sudo apt-get upgrade`

## Mass Storage

Below is to create a 2GB storage container:

```bash
echo "dtoverlay=dwc2" | sudo tee -a /boot/config.txt
echo "dwc2" | sudo tee -a /etc/modules
sudo dd if=/dev/zero of=/histapi.bin bs=1MB count=2048
```

Format in FAT32, create a mount point:

```bash
sudo mkdosfs /histapi.bin -F 32 -I
sudo mkdir /mnt/histapi`
```
 
Mount on boot. Append the following line to `/etc/fstab`:

`/histapi.bin /mnt/histapi vfat users,umask=000 0 2`

Mount the volume.

`sudo mount -a`

Now activate the mass storage:

`sudo modprobe g_mass_storage  file=/histapi.bin stall=0 ro=1`

Once the RPI connected to the data port from another computer, you should see the files in 
the volume and be able add new ones.

Insert this line in `/etc/rc.local` to enable mass storage on startup.

To refresh and be able to access the new files view on the PI, it's 
required to remount the filesystem:

`sudo umount /histapi.bin && sudo mount /histapi.bin`
 
## Enabling audio

Append to `/boot/config.txt`:

    dtoverlay=pwm-2chan,pin=18,func=2,pin2=13,func2=4

You can find more info on PWM overlay with command `dtoverlay -h 
pwm-2chan` as well as in the [official BCM2835 
documentation](https://www.raspberrypi.org/documentation/hardware/raspberrypi/bcm2835/BCM2835-ARM-Peripherals.pdf)

To record audio, we use a USB mini microphone of [that 
kind](https://www.adafruit.com/product/3367).

On the software side, Histapi leverage sox to record and process 
audio:

    sudo apt-get install -y sox
    sudo apt-get install libsox-fmt-all


## Install python libraries

Histapi rely on [gpiozero](https://gpiozero.readthedocs.io/en/stable/). 
Installation is pretty straight-forward:

`sudo apt-get install -y python3-gpiozero`

And for streaming and mixing audio, we use 
[pygame](https://www.pygame.org/news). It can be installed with:

`sudo apt-cache show python3-pygame`


## Installing Histapi

1. Install Git & Zip : `sudo apt-get install git zip`
1. Clone the repo : `git clone https://gitlab.com/histapi/histapi.git`
1. Unzip maintitles.zip
1. Create the folder `mystories` (TODO - enhance this)


## Start Histapi upon boot

TODO

## Microphone level setup

TODO (use alsamixer and set the input level in way not to get 
distortion.)

## convert camelCase to snake_case
`sed -r ':loop; /.*\<([a-z]+([A-Z][a-z]+)+)\>.*/ { h; s//\1/; s/([A-Z])/_\l\1/g; G; s/(.*)\n(.*)\<[a-z]+([A-Z][a-z]+)+\>(.*)/\2\1\4/; b loop }'`
